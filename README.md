🤩 Free and Open Source Software  
💙 Firefox, Thunderbird, GNU/Linux workstations, privacy-oriented tools  
💼 Work as a full stack web developer and data manager in clinical bioinformatics environment  

This is my personal GitLab profile 🤓  